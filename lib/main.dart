import 'package:flutter/material.dart';
import 'FirstAssignment/UI_Screens/Landing_page.dart';
import 'SecondAssignment/UI_Component_Design/First_Design.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      home:  Landing_page(),
    );
  }
}
