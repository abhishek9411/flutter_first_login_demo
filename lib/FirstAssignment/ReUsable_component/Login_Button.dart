
import 'package:flutter/material.dart';

class Login_Button extends StatefulWidget {

  Function() onTap;
  final title;

  Login_Button({required this.title,required this.onTap});

  @override

  _Login_ButtonState createState() => _Login_ButtonState();
}

class _Login_ButtonState extends State<Login_Button> {
  @override
  Widget build(BuildContext context) {
      return Padding(
        padding: EdgeInsets.all(15),
        child: InkWell(
          onTap: widget.onTap,
          child: Container(
            height: 50,
            width: double.infinity,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25), color: Colors.white),
            child: Center(
                child: Text(
                  widget.title,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.normal),
                )),
          ),
        ),
      );
    }
}
