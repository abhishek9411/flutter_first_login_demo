import 'package:flutter/material.dart';

class DropDownMenu {

  showDropdownMenu(BuildContext context,List<String> items, Function(String) onTap) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: DropdownButton<String>(
            value: items[0] ?? "",
            onChanged: (newValue) {
              onTap(newValue ?? "");
              Navigator.of(context).pop();
            },
            items: items.map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),
        );
      },
    );
  }

}
