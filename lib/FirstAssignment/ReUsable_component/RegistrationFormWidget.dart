import 'package:flutter/material.dart';

class RegistrationFormWidget extends StatefulWidget {
  String title;
  IconData icon;
  bool isPassword;
  bool readonlyField;
  Function()? onTap;
  TextEditingController? textEditingController;

  RegistrationFormWidget({required this.title,required this.icon,required this.isPassword,required this.readonlyField,this.textEditingController = null, this.onTap = null});

  @override
  _RegistrationFormWidgetState createState() => _RegistrationFormWidgetState();
}

class _RegistrationFormWidgetState extends State<RegistrationFormWidget> {

  var boxDecoration = BoxDecoration(
      border: Border.all(
        color: Colors.white,
        width: 2,
      ),
      borderRadius: BorderRadius.circular(25));

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Container(
        height: 50,
        padding: EdgeInsets.only(left: 15, right: 50),
        decoration: boxDecoration,
        child: Row(
          children: [
            Icon(
              widget.icon,
              color: Colors.white,
            ),
            SizedBox(
              width: 15,
            ),
            Expanded(
                child: TextField(
                  readOnly: widget.readonlyField,
                  onTap: widget.onTap,
                  controller: widget.textEditingController,
                  style: TextStyle(color: Colors.white),
                  textAlign: TextAlign.center,
                  obscureText: widget.isPassword,
                  decoration: InputDecoration(
                    hintText: widget.title,
                    hintStyle: TextStyle(
                      color: Colors.white, // change hint color here
                    ),
                    border: InputBorder.none,
                  ),
                ))
          ],
        ),
      ),
    );;
  }
}
