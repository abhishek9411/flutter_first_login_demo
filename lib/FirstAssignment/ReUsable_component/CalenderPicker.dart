import 'dart:io' show Platform;
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class CalenderPicker {

   openCalenderPicker(BuildContext context,Function(DateTime) onSelect) {
    if (Platform.isAndroid) {
      _androidDatePicker(context,onSelect);
    } else if (Platform.isIOS) {
      _iosDatePicker(context,onSelect);
    } else {
      _iosDatePicker(context,onSelect);
    }
  }

   _iosDatePicker(BuildContext context,Function(DateTime) onSelect) {
    showCupertinoModalPopup(
        context: context,
        builder: (BuildContext builder) {
          return Container(
            height: MediaQuery.of(context).copyWith().size.height * 0.25,
            color: Colors.white,
            child: CupertinoDatePicker(
              mode: CupertinoDatePickerMode.date,
              onDateTimeChanged: (chosenDateTime) {
                onSelect(chosenDateTime);
                  // "${value.toLocal()}".split(' ')[0];
              },
              initialDateTime: DateTime.now(),
              minimumYear: 2005,
              maximumYear: 2073,
            ),
          );
        });
  }

   _androidDatePicker(BuildContext context,Function(DateTime) onSelect) async {
    var chosenDateTime;
    chosenDateTime = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2008),
      lastDate: DateTime(2073),
    );
    onSelect(chosenDateTime);

    // "${chosenDateTime.toLocal()}".split(' ')[0];
  }
}

