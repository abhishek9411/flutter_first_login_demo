import 'package:flutter/material.dart';

class SocialLogin_Button extends StatefulWidget {
  String title;
  String iconName;
  Color widgetBGColor;
  Color titleColor;
  bool isIconVisible;
  Color borderColor;

SocialLogin_Button({required this.title, required this.iconName,required this.widgetBGColor,required this.titleColor, required this.isIconVisible,required this.borderColor});


  @override
  _SocialLogin_ButtonState createState() => _SocialLogin_ButtonState();
}

class _SocialLogin_ButtonState extends State<SocialLogin_Button> {
  @override
  Widget build(BuildContext context) {
      return Padding(
        padding: EdgeInsets.all(10),
        child: InkWell(
          onTap: (){
            print('hello');
          },
          child: Container(
            height: 50,
            padding: EdgeInsets.only(left: 15, right: 50),
            decoration: BoxDecoration(
                color: widget.widgetBGColor,
                border: Border.all(
                  color: widget.borderColor,
                  width: 2,
                ),
                borderRadius: BorderRadius.circular(25)),
            child: Row(
              children: [
                Visibility(
                  visible: widget.isIconVisible,
                  child: Container(
                      height: 20,
                      width: 20,
                      color: Colors.transparent,
                      child: Image.asset(widget.iconName)
                  ),
                ),

                SizedBox(
                  width: 15,
                ),
                Expanded(
                    child: Text(widget.title,textAlign: TextAlign.center,style: TextStyle(color: widget.titleColor,fontWeight: FontWeight.bold,fontSize: 16),)
                )
              ],
            ),
          ),
        ),
      );
    }
}
