import 'dart:io';

import 'package:flutter/material.dart';

import 'Registration.dart';
import '../ReUsable_component/SocialLogin_Button.dart';

class SocialLoginPage extends StatefulWidget {
  const SocialLoginPage({Key? key}) : super(key: key);

  @override
  _SocialLoginPageState createState() => _SocialLoginPageState();
}

class _SocialLoginPageState extends State<SocialLoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/background.jpg'),
                  fit: BoxFit.cover)),
        ),
        Container(
          height: double.infinity,
          // color: Colors.black.withOpacity(0.3),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Colors.black.withOpacity(0.3),
                Colors.black.withOpacity(0.9),
              ],
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                "Welcome",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              ),
              Text(
                "Join Mr. BookWarm!",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.normal,
                    fontSize: 15),
              ),
              SocialLogin_Button(
                  title: 'Google',
                  iconName: 'assets/images/googleIcon.png',
                  widgetBGColor: Colors.white,
                  titleColor: Colors.black,
                  isIconVisible: true,
                  borderColor: Colors.transparent),
              SocialLogin_Button(
                  title: 'facebook',
                  iconName: 'assets/images/facebookIcon.png',
                  widgetBGColor: Color.fromRGBO(58, 88, 151, 1),
                  titleColor: Colors.white,
                  isIconVisible: true,
                  borderColor: Colors.transparent),
              SocialLogin_Button(
                  title: 'Login',
                  iconName: 'assets/images/googleIcon.png',
                  widgetBGColor: Colors.transparent,
                  titleColor: Colors.white,
                  isIconVisible: false,
                  borderColor: Colors.white),
              Center(
                  child: Text(
                'Don\'t have an account',
                style: TextStyle(color: Colors.white),
              )),
              Center(
                child: TextButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => RegistrationPage()));
                  },
                  child: Text('Create Account',
                      style: TextStyle(color: Colors.white)),
                ),
              ),
              SizedBox(
                height: 50,
              )
            ],
          ),
        ),
      ],
    ));
  }
}
