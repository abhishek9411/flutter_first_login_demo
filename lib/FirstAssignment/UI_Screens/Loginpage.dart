import 'package:flutter/material.dart';

import '../Constants/Constants.dart';
import '../ReUsable_component/Login_Button.dart';
import '../ReUsable_component/RegistrationFormWidget.dart';
import 'Registration.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  _validateField() {
    final bool emailValid = emailRegex.hasMatch(_emailController.text);
    if (!emailValid) {
      _showErrorMessage('Please Enter Valid Email Address');
    }

    else if (_passwordController.text.length < 6) {
      _showErrorMessage('Please Enter Valid Password');
    }
  }

  _showErrorMessage(String msg) {
    ScaffoldMessenger.of(context).clearSnackBars();
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(msg),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/images/background.jpg'),
                    fit: BoxFit.cover)),
          ),
          Container(
            height: double.infinity,
            // color: Colors.black.withOpacity(0.3),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Colors.black.withOpacity(0.3),
                  Colors.black.withOpacity(0.9),
                ],
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  "Welcome",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
                Text(
                  "Join Mr. BookWarm!",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 15),
                ),
                RegistrationFormWidget(
                    title: 'Email',
                    icon: Icons.email,
                    isPassword: false,
                    readonlyField: false,
                    textEditingController: _emailController),
                RegistrationFormWidget(
                    title: 'Password',
                    icon: Icons.key,
                    isPassword: true,
                    readonlyField: false,
                    textEditingController: _passwordController),
                Login_Button(
                    title: 'Login',
                    onTap: () {
                      _validateField();
                    }),
                Center(
                    child: Text(
                  'Don\'t have an account',
                  style: TextStyle(color: Colors.white),
                )),
                Center(
                  child: TextButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RegistrationPage()));
                    },
                    child: Text('Create Account',
                        style: TextStyle(color: Colors.white)),
                  ),
                ),
                SizedBox(
                  height: 50,
                )
              ],
            ),
          ),
        ],
      )),
    );
  }
}
