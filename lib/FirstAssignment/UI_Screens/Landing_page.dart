import 'package:flutter/material.dart';
import 'package:flutter_assignment/Assignment3/Screens/HotelDasboard_page.dart';
import 'package:flutter_assignment/Weather%20App/Screens/WeatherScreen.dart';

import '../../SecondAssignment/UI_Component_Design/First_Design.dart';
import 'Loginpage.dart';


class Landing_page extends StatelessWidget {

  List assignmentsList = ['Assignment-1','Assignment-2','Explore Hotel Screen','Weather App'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: GridView.builder(
              itemCount: assignmentsList.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2, crossAxisSpacing: 10, mainAxisSpacing: 10),
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: () {
                    switch (index) {
                      case 0:
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LoginPage()));
                        break;
                      case 1:
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => First_Design_Page()));
                        break;
                      case 2:
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => HotelDashboard_page()));
                        break;
                      case 3:
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => WeatherScreen()));
                        break;
                      default:
                        break;
                    }
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Color.fromRGBO(221, 233, 219, 1.0),
                    ),
                    child: Center(
                      child: Text(
                        assignmentsList[index],
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                            color: Colors.grey),
                      ),
                    ),
                  ),
                );
              })),
    );
  }
}
