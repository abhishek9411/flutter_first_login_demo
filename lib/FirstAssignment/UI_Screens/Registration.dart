import 'package:flutter/material.dart';

import '../ReUsable_component/CalenderPicker.dart';
import '../ReUsable_component/DropDownMenu.dart';
import '../ReUsable_component/Login_Button.dart';
import '../ReUsable_component/RegistrationFormWidget.dart';
import '../Constants/Constants.dart';

class RegistrationPage extends StatefulWidget {
  const RegistrationPage({Key? key}) : super(key: key);

  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  TextEditingController _dobController = TextEditingController();
  TextEditingController _genderController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _confirmPasswordController = TextEditingController();
  TextEditingController _cityController = TextEditingController();
  TextEditingController _mobileNumberController = TextEditingController();
  TextEditingController _collageController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _dobController.text = "${DateTime.now().toLocal()}".split(' ')[0];
    _genderController.text = 'Male';
  }

  void ValidateField() {
    final bool emailValid = emailRegex.hasMatch(_emailController.text);
    if (!emailValid) {
      _showErrorMessage('Enter valid email Address');
      return;
    }

    if (_cityController.text.isEmpty) {
      _showErrorMessage('Enter valid City Name');
      return;
    }

    if (_mobileNumberController.text.isEmpty ||
        _mobileNumberController.text.length < 10) {
      _showErrorMessage('Enter valid Mobile number');
      return;
    }

    if (_collageController.text.isEmpty) {
      _showErrorMessage('Enter valid Collage Name');
      return;
    }

    if (_passwordController.text.isEmpty ||
        _passwordController.text.length < 6) {
      _showErrorMessage('Enter valid Password');
      return;
    }

    if (_confirmPasswordController.text.isEmpty ||
        (_passwordController.text != _confirmPasswordController.text)) {
      _showErrorMessage('Entered passwords doesn\'t match');
      return;
    }

    _showErrorMessage('Registration successfully');
  }

  void _showErrorMessage(String message) {
    ScaffoldMessenger.of(context).clearSnackBars();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),

      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/images/background.jpg'),
                    fit: BoxFit.cover)),
          ),
          SingleChildScrollView(
            physics: ClampingScrollPhysics(),
            child: Container(
              height: 900,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.black.withOpacity(0.3),
                    Colors.black.withOpacity(0.9),
                  ],
                ),
              ),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      "Welcome",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    ),
                    Text(
                      "Join Mr. BookWarm!",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.normal,
                          fontSize: 15),
                    ),
                    RegistrationFormWidget(
                      title: 'Email',
                      icon: Icons.email,
                      isPassword: false,
                      readonlyField: false,
                      textEditingController: _emailController,
                    ),
                    RegistrationFormWidget(
                      title: 'City',
                      icon: Icons.location_city_outlined,
                      isPassword: false,
                      readonlyField: false,
                      textEditingController: _cityController,
                    ),
                    RegistrationFormWidget(
                      title: 'Mobile Number',
                      icon: Icons.phone,
                      isPassword: false,
                      readonlyField: false,
                      textEditingController: _mobileNumberController,
                    ),
                    RegistrationFormWidget(
                      title: 'College',
                      icon: Icons.school,
                      isPassword: false,
                      readonlyField: false,
                      textEditingController: _collageController,
                    ),
                    RegistrationFormWidget(
                      title: 'Password',
                      icon: Icons.key,
                      isPassword: true,
                      readonlyField: false,
                      textEditingController: _passwordController,
                    ),
                    RegistrationFormWidget(
                      title: 'Confirm Password',
                      icon: Icons.key,
                      isPassword: true,
                      readonlyField: false,
                      textEditingController: _confirmPasswordController,
                    ),
                    RegistrationFormWidget(
                        title: 'Gender',
                        icon: Icons.transgender_outlined,
                        isPassword: false,
                        readonlyField: true,
                        textEditingController: _genderController,
                        onTap: () {
                          final dropDownMenu = DropDownMenu();
                          dropDownMenu.showDropdownMenu(
                              context,
                              gender,
                              (value) => {
                                    setState(() {
                                      this._genderController.text = value;
                                      print(value);
                                    })
                                  });
                        }),
                    RegistrationFormWidget(
                        title: 'Date of Birth',
                        icon: Icons.calendar_month,
                        isPassword: false,
                        readonlyField: true,
                        textEditingController: _dobController,
                        onTap: () {
                          var calender = CalenderPicker();
                          calender.openCalenderPicker(
                              context,
                              (dateTime) => {
                                    setState(() {
                                      this._dobController.text =
                                          "${dateTime.toLocal()}".split(' ')[0];
                                      print(
                                          'date time ${dateTime.toLocal()}".split('
                                          ')[0]');
                                    })
                                    // print("${dateTime.toLocal()}".split(' ')[0])
                                  });
                        }),

                    // getRegistrationButton(),
                    Login_Button(title: "Register",
                      onTap: () {
                        ValidateField();
                      },
                    ),
                    Center(
                        child: Text(
                      'Already have an account',
                      style: TextStyle(color: Colors.white),
                    )),
                    Center(
                      child: TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text('Login',
                            style: TextStyle(color: Colors.white)),
                      ),
                    ),
                    SizedBox(
                      height: 50,
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      )),
    );
  }
}
