import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_assignment/Assignment3/Reusable_companent/SaerchBar_page.dart';
import '../Reusable_companent/BannerPoster.dart';
import '../Reusable_companent/Hotels.dart';
import '../Reusable_companent/MostpopularView.dart';
import '../Reusable_companent/SearchBar.dart';
import '../Reusable_companent/SecondTopstatus.dart';
import '../Reusable_companent/TopStatusActivity.dart';

class HotelDashboard_page extends StatefulWidget {
  const HotelDashboard_page({Key? key}) : super(key: key);

  @override
  _HotelDashboard_pageState createState() => _HotelDashboard_pageState();
}

class _HotelDashboard_pageState extends State<HotelDashboard_page> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        backgroundColor: Colors.grey[200],
        elevation: 0,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.notifications_on,
              color: Colors.black,
            ),
            onPressed: () {
              // handle search button tap
            },
          ),
        ],
        title: Text(
          'Explore',
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 15, right: 15),
        child: SingleChildScrollView(
          child: Column(
            children: [
              TopStatusActivity(),
              Searchbar_page(),
              SizedBox(height: 15),
              SecondTopStatus(),
              SizedBox(height: 15),
              Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    'Most Popular Now',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  )),
              SizedBox(height: 15),
              MostPopularView(),
              SizedBox(height: 15),
              MostPopularView(),
              SizedBox(height: 5),
              MostPopularView(),
              SizedBox(height: 50),
              BannerPoster(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Hotels',style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                  TextButton(onPressed: (){}, child: Text('See All',style: TextStyle(fontSize: 16,fontWeight: FontWeight.normal)))
                ],
              ),
              Hotelspage(),
              SizedBox(height: 30),

            ],
          ),
        ),
      ),
    );
  }

}
