import 'package:flutter/material.dart';

class Appbar_widget extends StatefulWidget {
  const Appbar_widget({Key? key}) : super(key: key);

  @override
  State<Appbar_widget> createState() => _Appbar_widgetState();
}

class _Appbar_widgetState extends State<Appbar_widget> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0, // set elevation to 0 to remove the shadow
      actions: [
        IconButton(
          icon: Icon(Icons.notifications_on,color: Colors.black,),
          onPressed: () {
            // handle search button tap
          },
        ),

      ],
      title: Text(
        'Explore',
        style: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
          color: Colors.black,
        ),
      ),
    );
  }
}
