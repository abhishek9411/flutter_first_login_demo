import 'package:flutter/material.dart';

class SearchBar extends StatefulWidget {
  const SearchBar({Key? key}) : super(key: key);

  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,

      ),

      height: 50,
      child: Row(
        children: [
          SizedBox(width: 10,),
          Icon(Icons.search),
          SizedBox(width: 10,),
          Expanded(
            child: TextField(
              decoration: InputDecoration(
                hintText: 'Search city',
                border: InputBorder.none,
              ),
            ),
          ),                  ],
      ),
    );
  }
}
