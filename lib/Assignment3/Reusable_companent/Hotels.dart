import 'package:flutter/material.dart';

class Hotelspage extends StatefulWidget {
  const Hotelspage({Key? key}) : super(key: key);

  @override
  _HotelspageState createState() => _HotelspageState();
}

class _HotelspageState extends State<Hotelspage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 320,
      child: Container(
        child: GridView.builder(
            itemCount: 4,
            scrollDirection: Axis.horizontal,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 1, mainAxisSpacing: 20, childAspectRatio: 1.2),
            itemBuilder: (context, index) {
              return Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: Colors.white,
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(16),
                  child: Stack(
                    children: [
                      Positioned(
                          right: 0,
                          bottom: 120,
                          left: 0,
                          top: 0,
                          child: Image.asset(
                            'assets/images/hotels2.jpeg',
                            fit: BoxFit.cover,
                          )),
                      Positioned(
                        right: 10,
                          top: 10,
                          child: Icon(Icons.heart_broken_rounded,color: Colors.white,)
                      ),

                      Positioned(
                          bottom: 10,
                          left: 5,
                          right: 5,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Polania place hotel',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16),
                              ),
                              SizedBox(
                                height: 6,
                              ),
                              Text(
                                'Wola, Warsaw',
                                style: TextStyle(
                                    fontWeight: FontWeight.normal, fontSize: 14),
                              ),
                              SizedBox(
                                height: 6,
                              ),
                              Row(
                                children: [
                                  Icon(
                                    Icons.star,
                                    color: Colors.blue,
                                  ),
                                  Icon(
                                    Icons.star,
                                    color: Colors.blue,
                                  ),
                                  Icon(
                                    Icons.star,
                                    color: Colors.blue,
                                  ),
                                  Icon(
                                    Icons.star,
                                    color: Colors.blue,
                                  ),
                                  Icon(
                                    Icons.star,
                                    color: Colors.blue,
                                  ),
                                  Text(
                                    '4.1 (188)',
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 6,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    '1.5 Kilometers away',
                                    style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        fontSize: 14),
                                  ),
                                  SizedBox(
                                    width: 20,
                                  ),
                                  Container(
                                    width: 80,
                                    height: 25,
                                    decoration: BoxDecoration(
                                        color: Colors.grey.shade400,
                                        borderRadius: BorderRadius.circular(6)),
                                    child: Center(
                                        child: Text(
                                      '\$25 night',
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    )),
                                  )
                                ],
                              )
                            ],
                          ))
                    ],
                  ),
                ),
              );
            }),
      ),
    );
  }
}
