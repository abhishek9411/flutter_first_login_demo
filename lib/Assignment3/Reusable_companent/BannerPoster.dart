import 'package:flutter/material.dart';

import 'BannerCities.dart';

class BannerPoster extends StatefulWidget {
  const BannerPoster({Key? key}) : super(key: key);

  @override
  _BannerPosterState createState() => _BannerPosterState();
}

class _BannerPosterState extends State<BannerPoster> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 500,
      color: Colors.transparent,
      child: Stack(
        children: [
          Positioned(
              left: 0,
              right: 0,
              top: 0,
              bottom: 0,
              child: Image.asset(
                'assets/images/hotel8.jpeg',
                fit: BoxFit.fill,
              )),
          Positioned(
              left: 10,
              top: 310,
              child: Text(
                'Poland',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 16),
              )),
          Positioned(
              left: 10,
              top: 335,
              child: Text(
                'A spellbinding cities where culture collide',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.normal,
                    fontSize: 14),
              )),
          Positioned(
            left: 10,
            bottom: 10,
            right: 10,
            child: BannerCities(),
          )
        ],
      ),
    );
  }
}
