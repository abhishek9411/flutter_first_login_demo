import 'package:flutter/material.dart';

import '../Model/BannerModel.dart';

class BannerCities extends StatefulWidget {
  const BannerCities({Key? key}) : super(key: key);

  @override
  _BannerCitiesState createState() => _BannerCitiesState();
}

class _BannerCitiesState extends State<BannerCities> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120,
      width: MediaQuery.of(context).size.width,
      child: GridView.builder(
        itemCount: hotelTopActivityModel.length,
        scrollDirection: Axis.horizontal,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 1, // number of columns in the grid
             crossAxisSpacing: 0, // horizontal space between each item
            mainAxisSpacing: 10, // vertical space between each item
            childAspectRatio: 1.25
        ),
        itemBuilder: (context,intex) {
          return Column(
            children: [
              ClipRRect(borderRadius: BorderRadius.circular(16),child: Image.asset(hotelTopActivityModel?[intex].image ?? ''))
                            ,SizedBox(height: 15,),
              Flexible(child: Text(hotelTopActivityModel[intex].name ?? '',style: TextStyle(color: Colors.white,fontSize: 14,fontWeight: FontWeight.bold),))
            ],
          );
        },
      ),
    );
  }
}
