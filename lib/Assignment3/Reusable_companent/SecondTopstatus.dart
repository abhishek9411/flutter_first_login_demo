import 'package:flutter/material.dart';

import '../Model/AddStoryActivity.dart';

class SecondTopStatus extends StatefulWidget {
  const SecondTopStatus({Key? key}) : super(key: key);

  @override
  _SecondTopStatusState createState() => _SecondTopStatusState();
}

class _SecondTopStatusState extends State<SecondTopStatus> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      width: MediaQuery.of(context).size.width,
      child: GridView.builder(
          itemCount: addStoryActivityModel.length,
          scrollDirection: Axis.horizontal,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 1,
              mainAxisSpacing: 10,
              childAspectRatio: 2.0),
          itemBuilder: (context, index) {
            if (index == 0) {
              return Container(
                height: 200,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: Colors.white,

                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(16.0),
                  child: Stack(
                    children: [
                      Positioned(
                        height: 190,
                        left: 0,
                        right: 0,
                        child: Image.asset(
                          addStoryActivityModel[index].image ?? '',
                          fit: BoxFit.cover,
                        ),
                      ),
                      Positioned(
                        bottom: 30,
                        left: 25,
                        right: 25,
                        child: Center(
                            // alignment: Alignment.center,
                            child: Text(
                          'Create Story',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 18),
                        )),
                      ),
                      Positioned(
                          bottom: 80,
                          left: 25,
                          right: 25,
                          child: Center(
                            child: FloatingActionButton(
                              onPressed: () {},
                              child: Icon(Icons.add),
                            ),
                          ))
                    ],
                  ),
                ),
              );
            } else {
              return Container(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(16),
                  child: Stack(
                    children: [
                      Positioned(
                        bottom: 0,
                        top: 0,
                        child: Image.asset(
                          addStoryActivityModel[index].image ?? '',
                          fit: BoxFit.cover,
                        ),
                      ),
                      Positioned(
                        left: 10,
                        top: 10,
                        child: CircleAvatar(
                          radius: 30,
                          backgroundImage: AssetImage(
                              addStoryActivityModel[index].iconImage ?? ''),
                        ),
                      ),
                    ],
                  ),
                ),
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(15)),
              );
            }
          }),
    );
  }
}
