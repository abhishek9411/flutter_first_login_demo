import 'package:flutter/material.dart';

class Searchbar_page extends StatefulWidget {
  const Searchbar_page({Key? key}) : super(key: key);

  @override
  _Searchbar_pageState createState() => _Searchbar_pageState();
}

class _Searchbar_pageState extends State<Searchbar_page> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Colors.white,

      ),

      height: 50,
      child: Row(
        children: [
          SizedBox(width: 10,),
          Icon(Icons.search),
          SizedBox(width: 10,),
          Expanded(
            child: TextField(
              decoration: InputDecoration(
                hintText: 'Search city',
                border: InputBorder.none,
              ),
            ),
          ),                  ],
      ),
    );
  }

}

