import 'package:flutter/material.dart';

import '../Model/HotelsActivityModel.dart';

class TopStatusActivity extends StatefulWidget {
  const TopStatusActivity({Key? key}) : super(key: key);

  @override
  _TopStatusActivityState createState() => _TopStatusActivityState();
}

class _TopStatusActivityState extends State<TopStatusActivity> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120,
      width: MediaQuery.of(context).size.width,
      child: GridView.builder(
        itemCount: hotelTopActivityModel.length,
        scrollDirection: Axis.horizontal,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 1,
             crossAxisSpacing: 0,
            mainAxisSpacing: 10,
            childAspectRatio: 1.4
        ),
        itemBuilder: (context,intex) {
          return Column(
            children: [
              CircleAvatar(
                radius: 35,
                backgroundImage:
                AssetImage(hotelTopActivityModel?[intex].image ?? ''),
              ),
              SizedBox(height: 5,),
              Flexible(child: Text(hotelTopActivityModel[intex]?.name ?? '',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w700),maxLines: 1,))
            ],
          );
        },
      ),
    )
    ;
  }
}
