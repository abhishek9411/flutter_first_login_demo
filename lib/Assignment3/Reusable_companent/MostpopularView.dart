import 'package:flutter/material.dart';

import '../Model/MostPopularActivityModel.dart';

class MostPopularView extends StatefulWidget {
  const MostPopularView({Key? key}) : super(key: key);

  @override
  _MostPopularViewState createState() => _MostPopularViewState();
}

class _MostPopularViewState extends State<MostPopularView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            height: 110,
            child: GridView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: mostPopularActivityModel.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 1,
                    mainAxisSpacing: 10, // vertical space between each item
                    childAspectRatio: 0.32
                ), itemBuilder: (context,index){
              return Container(
                child: Column(
                  children: [
                    Flexible(
                      child: Row(
                        children: [
                          Container(
                              width: 150,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(16.0),
                                child: Image.asset(mostPopularActivityModel[index].image ?? '',
                                  fit: BoxFit.cover,
                                ),
                              )),
                          Flexible(
                            child: Container(
                              padding: EdgeInsets.only(left: 10),
                              child: Row(
                                children: [
                                  SizedBox(width: 20,child: Text('1',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16,color: Colors.grey))),
                                  
                                  Flexible(
                                    child: Column(
                                      mainAxisAlignment : MainAxisAlignment.center,
                                      children: [
                                        Text(mostPopularActivityModel[index].title ?? '',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),
                                        Align(alignment:Alignment.topLeft,
                                            child: Text(mostPopularActivityModel[index].subTitle ?? '',style: TextStyle(fontWeight: FontWeight.w400,fontSize: 14)))

                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 10,),
                    Divider(height: 5,color: Colors.grey,)

                  ],
                ),
              );
            }),
          )
        ],
      ),
    );
  }
}
