class MostPopularActivityModel {
  String? title;
  String? image;
  String? subTitle;

  MostPopularActivityModel({this.title,this.image,this.subTitle});
}

final mostPopularActivityModel = [MostPopularActivityModel(title: 'Place of Culture and science',image: 'assets/images/hotel1.jpeg',subTitle: 'Warsaw'),
  MostPopularActivityModel(title: 'Place of Culture and science',image: 'assets/images/hotel2.jpeg',subTitle: 'Warsaw'),
  MostPopularActivityModel(title: 'Place of Culture and science',image: 'assets/images/hotel3.jpeg',subTitle: 'Warsaw'),
  MostPopularActivityModel(title: 'Place of Culture and science',image: 'assets/images/hotel4.jpeg',subTitle: 'Warsaw'),
  MostPopularActivityModel(title: 'Place of Culture and science',image: 'assets/images/hotel5.jpeg',subTitle: 'Warsaw')];