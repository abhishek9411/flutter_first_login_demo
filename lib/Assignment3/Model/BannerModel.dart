class BannerModel {
  String? name;
  String? image;

  BannerModel({this.name,this.image});
}

final hotelTopActivityModel = [BannerModel(name: 'Royal',image: 'assets/images/hotel1.jpeg'),
  BannerModel(name: '5Star',image: 'assets/images/hotel2.jpeg'),
  BannerModel(name: '3Star',image: 'assets/images/hotel3.jpeg'),
  BannerModel(name: 'Paradise',image: 'assets/images/hotel4.jpeg'),
  BannerModel(name: 'JK tower',image: 'assets/images/hotel5.jpeg')];