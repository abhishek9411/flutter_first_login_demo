class HotelTopActivity {
  String? name;
  String? image;

  HotelTopActivity({this.name,this.image});
}

final hotelTopActivityModel = [HotelTopActivity(name: 'Hotels',image: 'assets/images/hotel1.jpeg'),
  HotelTopActivity(name: 'Flights',image: 'assets/images/hotel2.jpeg'),
  HotelTopActivity(name: 'Activities',image: 'assets/images/hotel3.jpeg'),
  HotelTopActivity(name: 'Restraurant',image: 'assets/images/hotel4.jpeg'),
  HotelTopActivity(name: 'Trains',image: 'assets/images/hotel5.jpeg')];