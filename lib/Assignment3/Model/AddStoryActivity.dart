class AddStoryActivityModel {
  String? iconImage;
  String? image;

  AddStoryActivityModel({this.iconImage,this.image});
}

final addStoryActivityModel = [AddStoryActivityModel(iconImage: 'assets/images/model5.jpeg',image: 'assets/images/model1.jpeg'),
  AddStoryActivityModel(iconImage: 'assets/images/model4.jpeg',image: 'assets/images/model2.jpeg'),
  AddStoryActivityModel(iconImage: 'assets/images/model3.jpeg',image: 'assets/images/model3.jpeg'),
  AddStoryActivityModel(iconImage: 'assets/images/model2.jpeg',image: 'assets/images/model4.jpeg'),
  AddStoryActivityModel(iconImage: 'assets/images/model1.jpeg',image: 'assets/images/model5.jpeg')];