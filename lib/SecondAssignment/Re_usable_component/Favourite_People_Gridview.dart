import 'package:flutter/material.dart';

import '../Constant/Constants.dart';
import '../Model/FavouritePeopleModel.dart';

class Favourite_people_page extends StatefulWidget {
  const Favourite_people_page({Key? key}) : super(key: key);

  @override
  _Favourite_people_pageState createState() => _Favourite_people_pageState();
}

class _Favourite_people_pageState extends State<Favourite_people_page> {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100, //MediaQuery.of(context).size.height,
      child: GridView.builder(
          itemCount: favouritePeopledataList.length,
          scrollDirection: Axis.horizontal,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 1,
              mainAxisSpacing: 5,
              crossAxisSpacing: 5,
              childAspectRatio: 1.2),
          itemBuilder: (context, index) {
            return getFavouritePeople(index, context);
          }),
    );
  }

  Widget getFavouritePeople(int index,BuildContext context) {
    if (index != 0) {
      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Stack(
              clipBehavior: Clip.none,
              children: [
                ClipOval(
                  child: Image.network(
                    favouritePeopledataList[index].profileUrl ?? '',
                    width: 70,
                    height: 70,
                    fit: BoxFit.cover,
                  ),
                ),
                Visibility(
                  visible: index != 0,
                  child: Positioned(
                    bottom: 7,
                    right: -7,
                    child: ClipOval(
                      child: Image.asset(
                        favouritePeopledataList[index].countryUrl, width: 22,
                        height: 22,
                        fit: BoxFit.cover,),
                    ),
                  ),
                )
              ],
            ),
            SizedBox(height: 10,),
            Text(favouritePeopledataList[index].name,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.normal)),
          ],
        ),
      );
    } else {
      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
                radius: 35,
                backgroundColor: Colors.black12,
                child: IconButton(
                  iconSize: 40,
                  icon: const Icon(Icons.add, color: Colors.grey,),
                  onPressed: () {
                    setState(() {
                      favouritePeopledataList.add(FavouritePeople(
                          name: 'Alex lara',
                          profileUrl: Constant.favouritePeopleURL,
                          countryUrl: 'assets/images/icon_india.png'));

                    });
                  },
                )
            ),
            SizedBox(height: 10,),
            Text(favouritePeopledataList[index].name,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.normal)),
          ],
        ),
      );
    }
  }
}
