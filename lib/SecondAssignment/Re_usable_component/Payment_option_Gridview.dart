import 'package:flutter/material.dart';

import '../Model/PaymentOptionModel.dart';

class Payment_option_GridView extends StatefulWidget {
  Function(int) onTap;

  Payment_option_GridView({ required this.onTap});

  @override
  _Payment_option_GridViewState createState() => _Payment_option_GridViewState();
}

class _Payment_option_GridViewState extends State<Payment_option_GridView> {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 330,//MediaQuery.of(context).size.height,
      child: GridView.builder(
          itemCount: paymentOptionList.length,
          physics: NeverScrollableScrollPhysics(),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 15,
              crossAxisSpacing: 15,
            childAspectRatio: 1.2
          ),
          itemBuilder: (context,index) {
            return InkWell(
              onTap: () => widget.onTap(index),
              child: Container(
                decoration: BoxDecoration(
                   borderRadius: BorderRadius.circular(15),
                  color: paymentOptionList[index].bgColor,
                ),
                child: Padding(
                  padding: EdgeInsets.only(left: 10,right: 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Icon(paymentOptionList[index].icons),
                      Text(paymentOptionList[index].title,style: TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.bold)),
                      Text(paymentOptionList[index].description,style: TextStyle(color: Colors.grey.shade700,fontSize: 16,fontWeight: FontWeight.normal)),

                    ],
                  ),
                ),
              ),
            );
          }
      ),
    );
  }
}
