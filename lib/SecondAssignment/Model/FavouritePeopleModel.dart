
import '../Constant/Constants.dart';

class FavouritePeople {

  final String name;
  final String? profileUrl;
  final String countryUrl;

  FavouritePeople(
      {required this.name,
        required this.profileUrl,
        required this.countryUrl});

}


final favouritePeopledataList = [
  FavouritePeople(
      name: 'Add',
      profileUrl: Constant.addURL,
      countryUrl: ''),
  FavouritePeople(
      name: 'Alex lara',
      profileUrl: Constant.favouritePeopleURL,
      countryUrl: 'assets/images/icon_india.png'),
  FavouritePeople(
      name: 'Alex lara',
      profileUrl: Constant.favouritePeopleURL,
      countryUrl: 'assets/images/icon_india.png'),
];
