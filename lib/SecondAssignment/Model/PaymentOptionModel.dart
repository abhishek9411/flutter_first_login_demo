import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PaymentOptionModel {
  final String title;
  final String description;
  final IconData icons;
  final Color bgColor;

  PaymentOptionModel(
      {required this.title,
      required this.description,
      required this.icons,
      required this.bgColor});
}

final paymentOptionList = [
  PaymentOptionModel(
      title: 'pay someone',
      description: 'To wallet , bank or mobile number',
      icons: Icons.payment,
      bgColor: Color.fromRGBO(245, 245, 253, 1.0)),
  PaymentOptionModel(
      title: 'Request money',
      description: 'Request money from orobopay users',
      icons: Icons.payments,
      bgColor: Color.fromRGBO(221, 233, 219, 1.0)),
  PaymentOptionModel(
      title: 'Buy airtime',
      description: 'Top-up or send airtime across africa',
      icons: Icons.paypal,
      bgColor: Color.fromRGBO(254, 250, 241, 1.0)),
  PaymentOptionModel(
      title: 'pay bill',
      description: 'Zero transaction fees when you pay',
      icons: Icons.outbox,
      bgColor: Color.fromRGBO(243, 244, 246, 1.0))
];
