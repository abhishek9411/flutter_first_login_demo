import 'package:flutter/material.dart';
import '../Re_usable_component/Favourite_People_Gridview.dart';
import '../Re_usable_component/Header_page.dart';
import '../Re_usable_component/Payment_option_Gridview.dart';


class First_Design_Page extends StatefulWidget {
  const First_Design_Page({Key? key}) : super(key: key);
  @override
  _First_Design_PageState createState() => _First_Design_PageState();
}

class _First_Design_PageState extends State<First_Design_Page> {
  String selectedOption = 'Option';

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Container(
            height: 700,
            child: Padding(
              padding: EdgeInsets.only(left: 15, right: 15),
              child: SingleChildScrollView(
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Header_page(),
                      SizedBox(
                        height: 15,
                      ),
                      Text('Here are some things you can do',
                          style: TextStyle(
                              color: Colors.grey.shade700,
                              fontSize: 16,
                              fontWeight: FontWeight.normal)),
                      SizedBox(
                        height: 15,
                      ),
                      Payment_option_GridView(onTap: (index) {
                        print(index);
                      }),
                      Text('Your favourites people',
                          style: TextStyle(
                              color: Colors.grey.shade700,
                              fontSize: 16,
                              fontWeight: FontWeight.normal)),
                      SizedBox(
                        height: 15,

                      ),
                      Favourite_people_page()

                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
