import 'dart:convert';
import 'dart:ffi';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:geolocator/geolocator.dart';
import 'package:geocoding/geocoding.dart';
import 'package:intl/intl.dart';

import '../Models/WaetherModel.dart';

enum Location {
  SelectCity,
  CurrentLocation,
  Delhi,
  Mumbai,
  Kanpur,
  Manali,
}

class WeatherScreen extends StatefulWidget {
  @override
  _WeatherScreenState createState() => _WeatherScreenState();
}

class _WeatherScreenState extends State<WeatherScreen> {
  WeatherDataModel? weatherModel;
  Location _selectedOption = Location.SelectCity;
  String? latitude;
  String? longitude;
  String? err;
  String? formattedDate;
  String? city;

  final TextStyle textStyle = TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.normal,
      color: Colors.white,
      fontStyle: FontStyle.italic);

  @override
  void initState() {
    super.initState();
    _getLocation();
    DateTime currentDate = DateTime.now();
    formattedDate =
        '${currentDate.day}-${currentDate.month}-${currentDate.year}';
  }

  String getTime(int timestamp) {
    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
    String formattedTime = DateFormat('h:mm a').format(dateTime);
    return formattedTime;
  }

  Future<void> _getLocation() async {
    try {
      LocationPermission permission = await Geolocator.checkPermission();
      if (permission == LocationPermission.denied ||
          permission == LocationPermission.deniedForever) {
        permission = await Geolocator.requestPermission();
      }

      if (permission == LocationPermission.whileInUse ||
          permission == LocationPermission.always) {
        Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high,
        );

        setState(() {
          latitude = '${position.latitude}';
          longitude = '${position.longitude}';
          print('latitude $latitude longitude $longitude');
          _getCityFromLatLng();
        });
      }
    } catch (e) {
      print('Abhishek:$e');
    }
  }

  Future<Map<String, dynamic>> fetchWeatherData(String city) async {
    final apiKey = '8b0d598bb58186c2859f6bb969ffae38';
    final apiUrl =
        'https://api.openweathermap.org/data/2.5/weather?q=$city&appid=$apiKey';

    final response = await http.get(Uri.parse(apiUrl));

    if (response.statusCode == 200) {
      // API call successful, parse the JSON response
      return json.decode(response.body);
    } else {
      // API call failed, handle the error
      setState(() {
        this.err = json.decode(response.body)['message'];
      });
      throw Exception(
          'Failed to fetch weather data :  ${json.decode(response.body)['message']}');
    }
  }

  Future<void> _getCityFromLatLng() async {
    try {
      List<Placemark> placemarks = await placemarkFromCoordinates(
        double.parse(this.latitude ?? ''),
        double.parse(this.longitude ?? ''),
      );

      if (placemarks.isNotEmpty) {
        setState(() {
          city = placemarks.first.locality;
          fetchWeather(city ?? '');
        });
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> fetchWeather(String city) async {
    try {
      final data = await fetchWeatherData(city);
      setState(() {
        this.weatherModel = WeatherDataModel.fromJson(data);
        getTime(this.weatherModel?.sys?.sunrise ?? 0);
      });
    } catch (e) {
      print('Error: $e');
      // Handle the error
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Weather App'),
      ),
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Colors.black38, Colors.black],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                getDropDownmenu(),
                getTempratureWidget(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget getTempratureWidget() {
    if (weatherModel != null) {
      return Column(
        children: [
          Align(
            alignment: Alignment.topRight,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(city ?? '', style: textStyle),
                Text(
                    '${((weatherModel?.main?.temp ?? 0.0) - 273.0).toStringAsFixed(2)}\'C',
                    style: TextStyle(
                        fontSize: 60,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontStyle: FontStyle.italic)),
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Text(formattedDate ?? '', style: textStyle),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                  'lat: ${((weatherModel?.coord?.lat ?? 0.0).toStringAsFixed(2))}',
                  style: textStyle),
              Text(
                  'lon: ${((weatherModel?.coord?.lon ?? 0.0).toStringAsFixed(2))}',
                  style: textStyle)
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                  'Min: ${(((weatherModel?.main?.tempMin ?? 0.0) - 273.09).toStringAsFixed(2))}',
                  style: textStyle),
              Text(
                  'Max: ${(((weatherModel?.main?.tempMax ?? 0.0) - 273.09).toStringAsFixed(2))}',
                  style: textStyle)
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Text(
                    'S_rise: ${((getTime(weatherModel?.sys?.sunrise ?? 0)))}',
                    style: textStyle),
              ),
              Text('S_set: ${(getTime(weatherModel?.sys?.sunset ?? 0))}',
                  style: textStyle)
            ],
          )
        ],
      );
    } else if (this.err != null) {
      return Padding(
        padding: const EdgeInsets.all(20.0),
        child: Text('Error: $err',
            style: TextStyle(fontSize: 20, color: Colors.red)),
      );
    } else {
      return CircularProgressIndicator();
    }
  }

  Widget getDropDownmenu() {
    return Container(
      child: DropdownButton(
        value: _selectedOption,
        items: Location.values
            .map(
              (value) => DropdownMenuItem(
                value: value,
                child: Text(
                  value.name,
                  style: const TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontStyle: FontStyle.italic),
                ),
              ),
            )
            .toList(),
        onChanged: (value) {
          if (value == null) return;
          this.err = null;
          setState(() {
            if (Location.SelectCity == value) {
              return;
            }
            if (Location.CurrentLocation != value) {
              this.city = value.name;
              _selectedOption = value;
              weatherModel = null;
              fetchWeather(value.name);
            } else {
              _selectedOption = value;
              weatherModel = null;
              _getCityFromLatLng();
            }
          });
        },
        dropdownColor: Colors.white,
        icon: const Icon(
          Icons.arrow_drop_down,
          color: Colors.white,
        ),
      ),
    );
  }
}
